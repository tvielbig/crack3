#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "bintree.h"

void insert(char *h, char *p, node **leaf)
{
    if (*leaf == NULL)
    {
        *leaf = (node *) malloc(sizeof(node));
        strcpy((*leaf)->hash,  h);
        strcpy((*leaf)->plain, p);
        /* initialize the children to null */
        (*leaf)->left = NULL;    
        (*leaf)->right = NULL;  
    }
    else if(strcmp(h, (*leaf)->hash)<0) {
        insert(h,p, &((*leaf)->left) );
    } 
    else if(strcmp(h, (*leaf)->hash)>0) {
        insert(h,p, &((*leaf)->right) );
    }
}

void print(node *leaf, int count)
{
	if (leaf == NULL) 
	{
	return;
	}
	if (leaf->left != NULL)	print(leaf->left, count+1);
	printf("%s - %s\n", leaf->hash, leaf->plain);
	if (leaf->right != NULL) print(leaf->right, count+1);
	printf("%s - %s\n", leaf->hash, leaf->plain);
}

// TODO: Modify so the key is the hash to search for
node *search(char *key, node *leaf)
{
  if (leaf != NULL)
  {
      int compare = strcmp(key,leaf->hash);
      if (compare == 0)
      { 
            return leaf;
      } 
      else if (compare < 0 )
      {
            return search(key, leaf->left);
      } 
      else 
      {
            return search(key, leaf->right);
      }
  }
  else return NULL;
}
