// DONE: modify the struct so it holds both the plaintext
// word and the hash.
typedef struct node {
	char hash[33];
	char plain[20];
	struct node *left;
	struct node *right;
} node;

void insert(char *hash, char *plain, node **leaf);

void print(node *leaf, int count);

node *search(char *hash, node *leaf);
