#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"
#include "bintree.h"

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    int result;
    // Hash the guess using MD5
    char *ph = md5(guess, strlen(guess));
    // Compare the two hashes
    result = strcmp(ph, hash);
    // Free any malloc'd memory
    free(ph);
    if(result == 0) 
    { 
        return 1;
    } else 
    {
        return 0;
    }
}

// DONE
// Read in the hash file and return the array of strings.
// Use the technique we showed in class to expand the
// array as you go.
char **read_hashes(char *filename)
{
    FILE *f = fopen(filename, "r");
    if(!f) printf("can't open %s for reading\n", filename);
    
    int lineCount = 0;
    char str[HASH_LEN];
    while(fscanf(f,"%s\n", str) != EOF) {
        lineCount++;
    }
    rewind(f);
    
    char **hashArray =(char **) malloc(sizeof(char *)*lineCount+1);
    
    int index = 0;
    while(fscanf(f, "%s\n", str) != EOF)
    {
        hashArray[index] = malloc(HASH_LEN);
        strcpy(hashArray[index], str);
        index++;
    }    
    hashArray[lineCount] = NULL;
    
    fclose(f);
    
    return hashArray;
}


// DONE
// Read in the dictionary file and return the tree.
// Each node should contain both the hash and the
// plaintext word.
node *read_dict(char *filename)
{
    FILE *f = fopen(filename, "r");
    if(!f) printf("can't open %s for reading\n", filename);
    
    struct node *root = NULL;
    
    char pw[PASS_LEN];
    char ph[HASH_LEN];
    while(fscanf(f,"%s\n", pw) != EOF) 
    {
        strcpy(ph, md5(pw, strlen(pw)));
        insert(ph, pw, &root);
    }
    
    return root;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // DONE: Read the hash file into an array of strings
    char **hashes = read_hashes(argv[1]);


    // DONE: Read the dictionary file into a binary tree
    node *dict = read_dict(argv[2]);

    // DONE
    // For each hash, search for it in the binary tree.
    // If you find it, get the corresponding plaintext dictionary
    // entry. Print both the hash and word out.
    // Need only one loop. (Yay!)
    int index = 0;
    node *match = NULL;
    while(hashes[index]) 
    {
        match = search(hashes[index], dict);
        if(match != NULL) 
        {   
            printf("hash:%s     pass:%s\n", match->hash, match->plain);
            match = NULL;
        } else printf("NULL\n");
        
        index++;
    }
}
